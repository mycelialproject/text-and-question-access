 <?php 
  /* This script strictly mirrors the behavior of a prior script that 
     multiple Processing sketches are relying on for the video projection 
     of participants texts. It cannot be changed nutil the DB changes, and 
     the Processing sketches change.
     */
    
	$dir = 'sqlite:/home/mycelial/logger/mycelial.db';
	$dbh  = new PDO($dir) or die("cannot open the database");
	$query =  "SELECT * FROM texts LIMIT 150;";

	$messages = array();

	// {"status":1,"data":[{"messageID":"6563","unityUserID":"b6ebef92-5263-4991-96bb-0eeb1d311def","tag":"LQ1","message":"Flowers in a field\n\n|Arty Gal","approved":"1"},
	foreach ($dbh->query($query) as $row)
	{
		// We now have each row. We want to create an array of messages.

		// mid fingerprint  timestamp tag content
		// mid fingerprint  timestamp tag content
		$q = "SELECT name FROM users WHERE fingerprint = '" . $row[1] . "';";
		$r = $dbh->query($q);
		$name = $r->fetchColumn(0);
		if ($name !== false) {
			$name = "anonymous";
		}
		$h = array();
		$h["messageID"] = $row[0];
		$h["unitUserID"] = $row[1];
		$h["tag"] = $row[3];
		$h["message"] = $row[4] . " - " . $name;
		$h["approved"] = "1";
		array_push($messages, $h);		
	}

	$resp = array();
	$resp["status"] = 1;
	$resp["data"] = $messages;
	$resp["state"] = 0;
	echo json_encode($resp);

  //This is how you close a PDO connection
	$dbh = null; 
?> 
